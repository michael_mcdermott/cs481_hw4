﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HW4ListView
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public static double Progress { set; get; }
        public static IList<TaskClass> TodoList { get; set; }
        public MainPage()
        {
           
            TodoList = new ObservableCollection<TaskClass>()
            {
                new TaskClass
                {
                    title = "First Task",
                    priority = "low",
                    notes = "do this to accomplish task 1",
                    howLong = "5"
                },
                new TaskClass
                {
                    title = "Task2",
                    priority = "low",
                    notes = "do this to accomplish task 2",
                    howLong = "20"
                },
                new TaskClass
                {
                    title = "Third Task",
                    priority = "medium",
                    notes = "do this to accomplish task 3",
                    howLong = "20"
                },
                new TaskClass
                {
                    title = "Task4",
                    priority = "high",
                    notes = "do this to accomplish task 4",
                    howLong = "15",
                    subTask = new TaskClass{title = "task4subtask", priority = "low",notes = "subtask notes", howLong="0" }
                }
            };

            Progress = 1 / (TodoList.Count + 1);

            InitializeComponent();
            TodoListView.ItemsSource = TodoList;
        }
        public void  DeleteTask(object sender, EventArgs e)
        {
            var todoItem = ((MenuItem)sender);
            var task = (TaskClass)todoItem.CommandParameter;
            TodoList.Remove(task);
           // return Task.Delay(100);
        }

        async void AddTask(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new TaskPage{
                BindingContext = new TaskClass()
            });
        }

        async void TaskSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem != null)
            {
                await Navigation.PushAsync(new TaskPage
                {
                    BindingContext = e.SelectedItem as TaskClass
                });
            }
        }
    }
}

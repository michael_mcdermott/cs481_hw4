﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace HW4ListView
{
    public partial class TaskPage : ContentPage
    {
        public TaskPage()
        {
            InitializeComponent();
        }


        /*async void OnSaveClicked(object sender, EventArgs e)
        {
            var taskClass = (TaskClass)BindingContext;
            OnPropertyChanged(nameof(taskClass.title));
            OnPropertyChanged(nameof(taskClass.notes));
            OnPropertyChanged(nameof(taskClass.howLong));
            OnPropertyChanged(nameof(taskClass.priority));
            OnPropertyChanged(nameof(taskClass.subTask));
            await Navigation.PopAsync();
        }*/

        async void OnAddClicked(object sender, EventArgs e)
        {
            var taskClass = (TaskClass)BindingContext;
            MainPage.TodoList.Add(taskClass);
            await Navigation.PopAsync();
        }

        async void OnRemoveClicked(object sender, EventArgs e)
        {
            var taskClass = (TaskClass)BindingContext;

           // var todoItem = ((MenuItem)sender);
           // var task = (TaskClass)todoItem.CommandParameter;
            MainPage.TodoList.Remove(taskClass);
            await Navigation.PopAsync();
        }

        async void OnCancelClicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }
    }
}

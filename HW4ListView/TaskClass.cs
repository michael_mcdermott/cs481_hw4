﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace HW4ListView
{
    public class TaskClass : INotifyPropertyChanged
    {
        
        
       public String title { get; set; }
       public String priority { get; set; } //0 for lowest, the higher the number the higher priority, ideally, 0-2 for low medium high
       public int actualPriority; // numeric value for how it actually works under the hood. easier than doing conversion

        public event PropertyChangedEventHandler PropertyChanged;

        public String notes { get; set; }
       public String howLong { get; set; } // estimated time to complete task in minutes
       public TaskClass subTask { get; set; }


        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        protected bool SetProperty<T>(ref T storage, T value, [CallerMemberName]string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(storage, value))
            {
                return false;
            }
            storage = value;
            OnPropertyChanged(propertyName);

            return true;
        }
        
    }
}
